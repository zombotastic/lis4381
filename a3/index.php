<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Alexander Boehm">
    <link rel="icon" href="favicon.ico">

		<title>CRSXXXX - Assignment1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> In this assignment we created a database and an erd. We also created the My Events App, and
					continued with our Skill Sets.
				</p>

				<h4 class="page-header">ERD</h4>
				<img src="img/erd.png" class="img-responsive center-block" alt="ERD">

				<h4 class="page-header">My Events</h4>
				<img src="img/myeventsa.png" class="img-responsive center-block" alt="My Events part.1">
				<img src="img/myeventsb.png" class="img-responsive center-block" alt="My Events part.2">


				<h4 class="page-header">Skill Set 4</h4>
				<img src="img/ss4.png" class="img-responsive center-block" alt="VS Code SS4">

				<h4 class="page-header">Skill Set 5</h4>
				<img src="img/ss5.png" class="img-responsive center-block" alt="VS Code SS5">

				<h4 class="page-header">Skill Set 6</h4>
				<img src="img/ss6.png" class="img-responsive center-block" alt="VS Code SS6">
			
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		 
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
