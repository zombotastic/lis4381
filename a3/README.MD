# LIS4381 - Mobile Web Application Development

## Alexander C. Boehm

### Assignment 3 Requirements:

*three Parts:*

1. Backwards-engineer an app which includes one activity, and includes multiple views
2. Include screenshots of the app
3. Include ERD and database links
4. Skill sets four, five, and six

#### Screenshots of My Event Application Running ADS:

| app part 1 | app part 2|
--- | ---
|![My Events a](img/myeventsa.png "My Events a")|![My Events a](img/myeventsb.png "My Events a")|

#### Skill Sets Four, Five, & Six

|Skill Set Four | Skill Set Five | Skill Set Six |
---|---|---
|![SS4 VS Code](img/ss4.png "SS4 VS Code")|![SS5 VS Code](img/ss5.png "SS5 VS Code")|![SS6 VS Code](img/ss6.png "SS6 VS Code")|


| ERD link .MWB | Database link .SQL |
---|---
| [ERD Link](docs/a3.mwb "ERD .MWB file") | [ERD Link](docs/a3.sql "ERD .MWB file") |
![ERD Image](img/erd.png "ERD Image") 