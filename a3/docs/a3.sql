-- MySQL Script generated by MySQL Workbench
-- Thu Oct  8 12:34:48 2020
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema ab16m
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `ab16m` ;

-- -----------------------------------------------------
-- Schema ab16m
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ab16m` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
SHOW WARNINGS;
USE `ab16m` ;

-- -----------------------------------------------------
-- Table `ab16m`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ab16m`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `ab16m`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) UNSIGNED NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_vtd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(225) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `ab16m`.` customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ab16m`.` customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `ab16m`.` customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cuz_zip` INT UNSIGNED NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(8,2) NOT NULL,
  `cus_total_sales` DECIMAL(8,2) UNSIGNED NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `ab16m`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ab16m`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `ab16m`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_price` DECIMAL(6,2) UNSIGNED NOT NULL,
  `pet_age` TINYINT UNSIGNED NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NOT NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `ab16m`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `ab16m`.` customer` (`cus_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `ab16m`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `ab16m`;
INSERT INTO `ab16m`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'Lab Laboratory', '234 Main Rd.', 'Tallahassee', 'FL', 32303, 18507774747, 'Labsouremail@aol.com', 'https://labslaboratory', 40000, NULL);
INSERT INTO `ab16m`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'Dog House', '1780 N Ruff St.', 'St. Pete', 'FL', 32304, 19572788909, 'Doggydoggy@aol.com', 'https://doghouse', 41000, NULL);
INSERT INTO `ab16m`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Planet', '876 Dynamo Rd.', 'Fort Lauderdale', 'FL', 32305, 17778737327, 'Pet123planet@aol.com', 'https://petplanet', 42000, NULL);
INSERT INTO `ab16m`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'Animal Zone', '8874 Yermas House Dr.', 'Key West', 'FL', 32303, 18237824755, 'RichardOsburne@proxy.com', 'https://animalzone', 43000, NULL);
INSERT INTO `ab16m`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'Parot Paradise', '664 Sticky Rd.', 'St. Augustine', 'FL', 32306, 17348753896, 'Parotdise@aol.com', 'https://parotparadise', 44000, NULL);
INSERT INTO `ab16m`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'Parakeets and Puppies', '5747 Drum Lane', 'Port Charolette', 'FL', 32307, 12736785832, 'Pupkeets@aol.com', 'https://keetsandpups', 45000, NULL);
INSERT INTO `ab16m`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'Lil Critters', '665 Martin Luther King St.', 'Sarasota', 'FL', 32308, 19418374738, 'LilCritters@aol.com', 'https://lilcritters', 46000, NULL);
INSERT INTO `ab16m`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'Giant Bois', '1700 N Monroe St.', 'Tallahassee', 'FL', 32309, 18506576864, 'GiantBois@aol.com', 'https://bigolbois', 47000, NULL);
INSERT INTO `ab16m`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'Rhino Retreat', '1616 Pepper Ave.', 'Tampa', 'FL', 32301, 16768944678, 'RhinosRUs@aol.com', 'https://rhinoretreat', 48000, NULL);
INSERT INTO `ab16m`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_vtd_sales`, `pst_notes`) VALUES (DEFAULT, 'House of Wolves', '850 E Tennessee.', 'Tallahassee', 'FL', 32302, 19504586942, 'Canenineshop@aol.com', 'https://wolvehoiuse', 49000, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ab16m`.` customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `ab16m`;
INSERT INTO `ab16m`.` customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cuz_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Steve', 'Jobs', '336 Residential Rd.', 'Tallahassee', 123532, 1391234522, 'Steveo@aol.com', 10.23, 300.0, 'note1');
INSERT INTO `ab16m`.` customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cuz_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Harry', 'Potter', '568 House Lane', 'St. Pete', 234343, 1123124324, 'Pottys@aol.com', 23.56, 200.65, 'note2');
INSERT INTO `ab16m`.` customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cuz_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Justin', 'Rosenthall', '5683 Timber Oaks', 'Tallahassee', 324567, 1233242355, 'KavaKava@aol.com', 27.0, 500.0, 'note3');
INSERT INTO `ab16m`.` customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cuz_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Jacob', 'Justice', '5437 Garden Center', 'Tallahassee', 123385, 1236436544, 'Jesusis1@aol.com', 5.00, 342.0, 'note4');
INSERT INTO `ab16m`.` customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cuz_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'MacKenzie', 'Belle', '6675 Old Pl. ', 'Las Vegas', 476479, 2344365454, 'anonymous@aol.com', 123.42, 590.99, 'note5');
INSERT INTO `ab16m`.` customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cuz_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Jessica', 'Rabbi', '357 Warp Dr.', 'L.A.', 345734, 4353456546, 'loonytunes@aol.com', 1532.33, 4230.0, 'note6');
INSERT INTO `ab16m`.` customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cuz_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Berry', 'Button', '538 This Pl.', 'New Orleans', 342554, 2342433434, 'buttons@aol.com', 235.22, 500.34, 'note7');
INSERT INTO `ab16m`.` customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cuz_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Bill', 'Boller', '481C That Dr.', 'New York', 345475, 2434354566, 'MidtownKava@aol.com', 89.0, 200.0, 'note8');
INSERT INTO `ab16m`.` customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cuz_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Walter', 'Boehm', '5932 Some Lane', 'St. Pete', 234354, 4634523444, 'Thisgui@aol.com', 69.45, 590.54, 'note9');
INSERT INTO `ab16m`.` customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cuz_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Brian', 'Boehm', '4364 Goose St.', 'Wisconsin', 435345, 3434343434, 'Brobro@aol.com', 260, 1000.0, 'note10');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ab16m`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `ab16m`;
INSERT INTO `ab16m`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 2, 'labrador', 'm', 250, 350, 5, 'blonde', '1998-07-03', 'y', 'y', 'note1');
INSERT INTO `ab16m`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 4, 'husky', 'm', 350, 400, 4, 'white', '2019-05-02', 'y', 'y', 'note2');
INSERT INTO `ab16m`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 6, 'tabby cat', 'f', 150, 200, 3, 'white and black', '2019-05-03', 'y', 'n', 'note3');
INSERT INTO `ab16m`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 8, 'chihuahua', 'f', 200, 300, 3, 'black', '2019-03-22', 'y', 'y', 'note4');
INSERT INTO `ab16m`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 10, 'paraot', 'm', 150, 300, 3, 'red', '2019-09-15', 'n', 'n', 'note5');
INSERT INTO `ab16m`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 7, 1, 'fox', 'f', 300, 500, 2, 'brown', '2019-12-17', 'y', 'n', 'note6');
INSERT INTO `ab16m`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 6, 3, 'gecko', 'm', 80, 130, 1, 'green', '2019-11-20', 'n', 'n', 'note7');
INSERT INTO `ab16m`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 8, 5, 'bearded dragon', 'm', 140, 200, 3, 'green', '2019-02-13', 'n', 'n', 'note8');
INSERT INTO `ab16m`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 10, 7, 'ferret', 'm', 100, 150, 2, 'brown', '2019-08-20', 'y', 'n', 'note9');
INSERT INTO `ab16m`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 9, 9, 'hamster', 'f', 20, 70, 4, 'brown', '2019-12-30', 'y', 'n', 'note10');

COMMIT;

