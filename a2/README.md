# LIS4381 - Mobile Web Application Development

## Alexander C. Boehm

### Assignment 2 Requirements:

*three Parts:*

1. Backwards-engineer an app which includes two activities, and includes multiple views
2. Screenshots of the program
3. Skill sets one, two, and three

#### Screenshots of Bruschetta application running ADS:

| Part 1 | Part 2|
--- | ---
|![My Events a](img/bruschetta1.png "My Events a")|![My Events a](img/bruschetta2.png "My Events a")|

#### Skill sets One, Two, & Three

|Skill Set Four | Skill Set Five | Skill Set Six |
---|---|---
|![SS1 VS Code](img/ss1.png "SS1 VS Code")|![SS2 VS Code](img/ss2.png "SS2 VS Code")|![SS3 VS Code](img/ss3.png "SS3 VS Code")|
|![SS2 EXTRA](img/ss2constructor.png "SS2 EXTRA")|![SS2 EXTRA](img/ss2main.png "SS2 EXTRA")|
