<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Alexander Boehm">
    <link rel="icon" href="favicon.ico">

		<title>CRSXXXX - Assignment1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> In this assignment we created our first real mobile application, Bruschetta Recipe. 
					We also began our first Skill Sets. 
				</p>

				<h4 class="page-header">Bruschetta App</h4>
				<img src="img/bruschetta1.png" class="img-responsive center-block" alt="Bruschetta App">
				<img src="img/bruschetta2.png" class="img-responsive center-block" alt="Bruschetta App 2">

				<h4 class="page-header">Skill Set 1</h4>
				<img src="img/ss1.png" class="img-responsive center-block" alt="VS Code ss1">

				<h4 class="page-header">Skill Set 2</h4>
				<img src="img/ss2.png" class="img-responsive center-block" alt="VS Code ss2">

				<h4 class="page-header">Skill Set 3</h4>
				<img src="img/ss3.png" class="img-responsive center-block" alt="VS Code ss3">
			
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		 
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
