# LIS4381 - Mobile Web Application Development

## Alexander C. Boehm

### Project 2 Requirements:

*Three Parts:*

1. Finalize the Database Project by Adding The Update and Delete Functions
2. Provide Screenshots
3. Create a RSS Link

#### Screenshots of Project 2:

![Index Page](img/index.png "Index Page")|
![Edit Page](img/edit.png "Edit Page")|
![Edit Failed](img/failed.png "Edit Failed")|
![Edit Passed](img/editpass.png "Edit Passed")|
![Delete Prompt](img/deleteprompt.png "Delete Prompt")|
![Deleted](img/deleted.png "Deleted")|
![Test Page](img/test.png "Test Page")|

### link to local host
[Local Host](http://localhost/repo/lis4381/p2/index.php) "LIS 4381 Repo"