# LIS4381 - Mobile Web Application Development

## Alexander C. Boehm

### Assignment 5 Requirements:

*Two Parts:*

1. Create a native application which provides server-side data validation and can add entries to a mySQL DB
2. Skill sets thirteen, fourteen, and fifteen

#### Screenshots of Assignnment 5:

### Server-Side Validation Screenshots
![Server-Side Validation A](img/a5a.png "Server-Side Validation A")|
![Server-Side Validation B](img/a5b.png "Server-Side Validation B")|
<b>added data entry</b>
![Server-Side Validation C](img/a5c.png "Server-Side Validation C")|

### Skill Set Thirteen

![SS13 VS Code](img/ss13.png "SS10 VS Code")

#### Skill Set Forteen
|Screenshot 1| Screenshot 2|
---|---
|![SS14 VS Code](img/ss14a.png "SS14 VS Code")|![SS14 VS Code](img/ss14b.png "SS14 VS Code")|
### link to local host
[Local Host](http://localhost/repo/lis4381/a5/index.php) "LIS 4381 Repo"