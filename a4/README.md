# LIS4381 - Mobile Web Application Development

## Alexander C. Boehm

### Assignment 4 Requirements:

*Three Parts:*

1. Create a native application which hosts a portfolio
3. Include a data validation page
4. Skill sets ten, eleven, and twelve

#### Screenshots of My Event Application Running ADS:

### portfolio home page
|![My Portfolio](img/a41.png "My Portfolio")|

### Data Validation Form
|![Validation Pass](img/validationpass.png "Validation Pass")|![Validation Fail](img/validationfail.png "Validation Fail")|

#### Skill Sets Four, Five, & Six

|Skill Set Ten | Skill Set Eleven |
---|---
|![SS10 VS Code](img/ss10.png "SS10 VS Code")|![SS11 VS Code](img/ss11.png "SS11 VS Code")|![SS12 VS Code]|

### link to local host
[Local Host](http://localhost/repo/lis4381/a4/index.php) "LIS 4381 Repo"