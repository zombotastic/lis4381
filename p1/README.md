# LIS4381 - Mobile Web Application Development

## Alexander C. Boehm

### Project 1 Requirements:

*Three Parts:*

1. Backwards-engineer an app which includes two activity, and includes multiple views
2. Include screenshots of the app
3. Skill sets seven, eight, and nine

#### Screenshots of My Event Application Running ADS:

| App Activity 1 | App Activity 2|
--- | ---
|![my_Buissness_Card A](img/p1a.png "My Buisness Card A")|![my_Buissness_Card B](img/p1b.png "My Buisness Card B")|

#### Skill Sets Seven, Eight, & Nine

|Skill Set Seven | Skill Set Eight | Skill Set Nine |
---|---|---
|![SS7 VS Code](img/ss7.png "SS7 VS Code")|![SS8 VS Code](img/ss8.png "SS8 VS Code")|![SS9 VS Code](img/ss9.png "SS9 VS Code")|

#### Link to BitBucket

[Main BitBucket Repo Link](https://bitbucket.org/zombotastic/lis4381/ "Main Repo Link")


