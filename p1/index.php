<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Alexander Boehm">
    <link rel="icon" href="favicon.ico">

		<title>CRSXXXX - Assignment1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> In this project we created a buisness card and and
					continued with our Skill Sets.
				</p>

				<h4 class="page-header">My Buisness Card</h4>
				<img src="img/p1a.png" class="img-responsive center-block" alt="My Buisness Card part.1">
				<img src="img/p1b.png" class="img-responsive center-block" alt="My Buisness Card part.2">


				<h4 class="page-header">Skill Set 7</h4>
				<img src="img/ss7.png" class="img-responsive center-block" alt="VS Code SS7">

				<h4 class="page-header">Skill Set 8</h4>
				<img src="img/ss8.png" class="img-responsive center-block" alt="VS Code SS8">

				<h4 class="page-header">Skill Set 9</h4>
				<img src="img/ss9.png" class="img-responsive center-block" alt="VS Code SS9">
			
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		 
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
