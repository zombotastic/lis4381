<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Alexander Boehm">
	<link rel="icon" href="favicon.ico">

	<title>CRSXXXX - SS14</title>
		<?php include_once("global/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">

					<h1>Simple Calculator</h2>
					<h3>Performs Addition, Subtraction, Multiplication, Division, and Exponentation</h3>

						<form id="defaultForm" method="post" class="form-horizontal" action="#">
								<div class="form-group">
										<label class="col-sm-4 control-label">Num1:</label>
										<div class="col-sm-4">
												<input type="text" id ="num1" class="form-control" name="num1" />
										</div>
								</div>
								<div class="form-group">
										<label class="col-sm-4 control-label">Num2:</label>
										<div class="col-sm-4">
												<input type="text" id="num2" class="form-control" name="num2" />
										</div>
								</div>
							</form>
			
						
			<div class ="starter-template" id = "bottom">
						<input type = "radio" id = "add" name = "operator" checked = "checked" value="+">
						<label for = "add">addition</label>
						<input type = "radio" id ="sub" name="operator" value="-">
						<label for = "sub">subtraction</label>
						<input type = "radio" id ="mul" name="operator" value="*">
						<label for = "mul">multiplication</label>
						<input type = "radio" id ="div" name="operator" value="/">
						<label for = "div">division</label>
						<input type ="radio" id ="expon" name="operator" value="*^">
						<label for = "expon">exponentiation</label>
						<br>
						<button>Calculate!</button>
						<br>
			</div>
			<?php include_once "global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->


	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../js/include_js.php"); ?>
			<?php include 'process.php';?>

</body>
</html>
