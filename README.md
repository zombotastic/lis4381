> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4381 - Mobile Web Application Development

## Alexander C. Boehm 

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md")
    - Install AMPSS (*ONLY* if not previously installed)
    - install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md")
    - Create Healthy Recipes Android app
    - Provide screenshots of completed app
    - Provide screenshots of skill sets one, two, & three

3. [A3 README.mm](a3/README.MD "My A3 README.md")
    - Create ED based upon buisness rules
    - Provide screenshot of completed ERD
    - Provide DB resource links
    - Create an app
    - Complete Skill Sets
  
4. [P1 README.md](p1/README.md "My P1 README.md")
    - Backwards-engineer an app which includes two activities, and includes multiple views
    - Include screenshots of the app
    - Skill sets Seven, Eight, and Nine
  
5. [A4 README.md](a4/README.md "My A4 README.md")
    - Create a native application which hosts a portfolio
    - Include a data validation page
    - Skill sets ten, eleven, and twelve
    
6. [A5 README.md](a5/README.md "My A5 README.md")
    - Create a native application which provides server-side data validation and can add entries to a mySQL DB
    - Skill sets thirteen, fourteen, and fifteen

7. [P2 README.md](p2/README.md "My P2 README.md")
    - Finalize the Database Project by Adding The Update and Delete Functions
    - Provide Screenshots
    - Create a RSS Link

